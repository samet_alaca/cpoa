package tests;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pojo.Client;

public class CSVReader {
	
	/**
	 * Parse CSV File and returns list of string
	 * @param file
	 * @param split
	 * @return List<String[]>
	 */
	public static List<String[]> parse(String file, String split) {
		String line = "";
		List<String[]> result = new ArrayList<String[]>();
		
		try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
			while((line = reader.readLine()) != null) {
				result.add(line.split(split));
			}
		} catch(IOException error) {
			error.printStackTrace();
		}
		
		return result;
	}
	
	public static List<Client> parseClients(String file) {
		List<Client> clients = new ArrayList<Client>();
		List<String[]> str = parse(file, ";");
		
		for(String[] line : str) {
			Client c = new Client(Integer.parseInt(line[0]), 
					line[1], 
					line[2], 
					Normalisation.no_voie(line[3]), 
					Normalisation.Voie(line[4]), 
					Normalisation.CodePostal(line[5]), 
					Normalisation.Ville(line[6]), 
					Normalisation.Pays(line[7]));
			
			clients.add(c);
		}
		
		return clients;
	}
}
