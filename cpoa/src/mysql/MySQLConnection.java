package mysql;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class MySQLConnection {
	
	private static Connection connection;
	
	public static Connection getInstance() {
		if(connection == null) {
			try {
				Properties database = new Properties();
				database.loadFromXML(new FileInputStream(new File("config/database.dtd")));
				connection = DriverManager.getConnection(
					"jdbc:mysql://" +
					database.getProperty("host") + 
					":" +
					database.getProperty("port") +
					"/" +
					database.getProperty("database"),
					database.getProperty("username"),
					database.getProperty("password")
				);
			} catch(Exception error) {
				error.printStackTrace();
				connection = null;
			}
		}
		return connection;
	}
	
}
