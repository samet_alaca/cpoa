package mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.DAO;
import pojo.Revue;

public class MySQLRevueDAO implements DAO<Revue> {
	
	private static MySQLRevueDAO instance;

	public static MySQLRevueDAO getInstance() {
		if(instance == null) {
			instance = new MySQLRevueDAO();
		}
		return instance;
	}

	@Override
	public Revue getById(int id) {
		try {
			String sql = "SELECT * FROM Revue WHERE id_revue = ?";
			PreparedStatement request = MySQLConnection.getInstance().prepareStatement(sql);
			request.setInt(1, id);
			
			ResultSet result = request.executeQuery();
			
			if(result.next()) {
				return new Revue(id,
					result.getString("titre"),
					result.getString("description"),
					result.getFloat("tarif_numero"),
					result.getString("visuel"),
					result.getInt("id_periodicite")
				);
			}
			
			if(request != null) {
				request.close();
			}
		} catch(SQLException error) {
			System.out.println("Erreur s�lection Revue : " + error.getMessage());
		}
		return null;
	}

	@Override
	public void create(Revue objet) {
		try {
			String sql = "INSERT INTO Revue (id_revue, titre, description, tarif_numero, visuel, id_periodicite) VALUES (?, ?, ?, ?, ?, ?)";
			PreparedStatement request = MySQLConnection.getInstance().prepareStatement(sql);
			request.setInt(1, objet.getId_revue());
			request.setString(2, objet.getTitre());
			request.setString(3, objet.getDescription());
			request.setDouble(4, objet.getTarif_numero());
			request.setString(5, objet.getVisuel());
			request.setInt(6, objet.getId_periodicite());
			
			request.executeUpdate();
			
			if(request != null) {
				request.close();
			}
		} catch (SQLException error) {
			System.out.println("Erreur cr�ation Revue : " + error.getMessage());
		}
	}

	@Override
	public void update(Revue objet) {
		try {
			String sql = "UPDATE Revue SET titre = ?, description = ?, tarif_numero = ?, visuel = ?, id_periodicite = ? WHERE id_revue = ?";
			PreparedStatement request = MySQLConnection.getInstance().prepareStatement(sql);
			request.setString(1, objet.getTitre());
			request.setString(2, objet.getDescription());
			request.setDouble(3, objet.getTarif_numero());
			request.setString(4, objet.getVisuel());
			request.setInt(5, objet.getId_periodicite());
			request.setInt(6, objet.getId_revue());
			
			request.executeUpdate();
			
			if(request != null) {
				request.close();
			}
		} catch(SQLException error) {
			System.out.println("Erreur modification Revue : " + error.getMessage());
		}
	}

	@Override
	public void delete(Revue objet) {
		try {
			String sql = "DELETE FROM Revue WHERE id_revue = ?";
			PreparedStatement request = MySQLConnection.getInstance().prepareStatement(sql);
			request.setInt(1, objet.getId_revue());
			
			request.executeUpdate();
			
			if(request != null) {
				request.close();
			}
		} catch(SQLException error) {
			System.out.println("Erreur suppression Revue : " + error.getMessage());
		}
	}

	@Override
	public List<Revue> getAll() {
		try {
			String sql = "SELECT * FROM Revue";
			PreparedStatement request = MySQLConnection.getInstance().prepareStatement(sql);
			List<Revue> l = new ArrayList<Revue>();
			
			ResultSet result = request.executeQuery();
			
			while(result.next()) {
				l.add(new Revue(result.getInt("id_revue"), result.getString("titre"), result.getString("description"), result.getFloat("tarif_numero"), result.getString("visuel"), result.getInt("id_periodicite")));
			}
			
			if(request != null) {
				request.close();
			}
			
			return l;
		} catch(SQLException error) {
			System.out.println("Erreur selection du tout Revue : " + error);
		}
		return null;
	}
}
