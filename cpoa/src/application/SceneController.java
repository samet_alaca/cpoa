package application;

import java.io.File;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import dao.DAO;
import factory.DAOFactory;
import factory.Persistance;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import mysql.MySQLConnection;

import pojo.*;
import tests.CSVReader;
import tests.Normalisation;

public class SceneController {
	
	@FXML private Button 
			el_btnSelectMYSQL, el_btnSelectLM, el_btnEditRevue, el_btnDeleteRevue, el_btnEditClient, 
			el_btnDeleteClient, el_btnEditAbo, el_btnDeleteAbo, el_btnEditPerio, el_btnDeletePerio;
	
	@FXML private TextField 
			el_inputIdRevue, el_inputTitreRevue, el_inputTarifRevue, el_inputIdClient, el_inputNomClient,
			el_inputPrenomClient, el_inputNorueClient, el_inputVoieClient, el_inputCodepostalClient,
			el_inputVilleClient, el_inputPaysClient, el_inputIdPerio, el_inputLabelPerio;
	
	@FXML private ImageView el_imgRevue;
	@FXML private ComboBox<String> el_inputPeriodiciteRevue, el_inputClientAbo, el_inputRevueAbo;
	@FXML private TableView<Revue> el_tabRevue;
	@FXML private TableView<Client> el_tabClient;
	@FXML private TableView<Abonnement> el_tabAbo;
	@FXML private TableView<Periodicite> el_tabPerio;
	@FXML private DatePicker el_inputDatedebAbo, el_inputDatefinAbo;
	@FXML private Label el_mysqlLabel;
	@FXML private TextArea el_inputDescriptionRevue;
	@FXML private TabPane el_tabpane;
	
	private Persistance persistance = null;
	private String revueVisuel = "";
	private boolean noDBConnection = false;
	
	// INIT BEG
	
	@FXML
	protected void initialize() {
		if(MySQLConnection.getInstance() == null) {
			el_mysqlLabel.setTextFill(Paint.valueOf("RED"));
			el_mysqlLabel.setText("Impossible de se connecter à la BDD");
			el_btnSelectMYSQL.setDisable(true);
			noDBConnection = true;
		}
	}
	
	@FXML
	public void selectMYSQL() {
		if(persistance == null) {
			enableTabs();
		}
		el_btnSelectLM.setDisable(false);
		el_btnSelectMYSQL.setDisable(true);
		persistance = Persistance.MYSQL;
		init();
	}
	
	@FXML
	public void selectLM() {
		if(persistance == null) {
			enableTabs();
		}
		el_btnSelectLM.setDisable(true);
		if(!noDBConnection) {
			el_btnSelectMYSQL.setDisable(false);
		}
		persistance = Persistance.LM;
		init();
	}
	
	private void enableTabs() {	
		el_tabpane.getTabs().forEach(tab -> {
			tab.setDisable(false);
		});
		SingleSelectionModel<Tab> selectionModel = el_tabpane.getSelectionModel();
		selectionModel.select(1);
	}
	
	private void init() {
		clearAll();
		loadRevuePerioBox();
		loadPerioClientBox();
		loadPerioRevueBox();
		initRevueTable();
		initClientTable();
		initAboTable();
		initPerioTable();
	}
	
	private void displayMessageBox(Alert.AlertType type, String title, String header, String content) {
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.showAndWait();
	}
	
	private void clearAll() {
		el_tabRevue.getItems().clear();
		el_tabClient.getItems().clear();
		el_tabAbo.getItems().clear();
		el_tabPerio.getItems().clear();
	}
	
	// INIT END
	// -------------------------
	// REVUE BEG
	
	private void loadRevuePerioBox() {
		List<Periodicite> perio = DAOFactory.getDAOFactory(persistance).getPeriodiciteDAO().getAll();
		List<String> perioStr = new ArrayList<String>();
		
		perio.forEach(p -> {
			perioStr.add(p.getLibelle());
		});
		el_inputPeriodiciteRevue.setItems(FXCollections.observableList(perioStr));
		SingleSelectionModel<String> selectionModel = el_inputPeriodiciteRevue.getSelectionModel();
		selectionModel.select(0);
	}
	
	@SuppressWarnings("unchecked")
	private void initRevueTable() {
		TableColumn<Revue, Integer> colPeriodicite = new TableColumn<Revue, Integer>("ID Periodicite");
		colPeriodicite.setCellValueFactory(new PropertyValueFactory<Revue, Integer>("id_periodicite"));
		
		TableColumn<Revue, String> colTitre = new TableColumn<Revue, String>("Titre");
		colTitre.setCellValueFactory(new PropertyValueFactory<Revue, String>("titre"));
		
		TableColumn<Revue, Float> colTarif = new TableColumn<Revue, Float>("Tarif");
		colTarif.setCellValueFactory(new PropertyValueFactory<Revue, Float>("tarif_numero"));
		
		el_tabRevue.getColumns().setAll(colTitre, colTarif, colPeriodicite);
		List<Revue> revues = DAOFactory.getDAOFactory(persistance).getRevueDAO().getAll();
		if(revues != null) {
			el_tabRevue.getItems().addAll(revues);
		}
	}
	
	private void updateRevueTable() {
		el_tabRevue.getItems().clear();
		el_tabRevue.getItems().addAll(DAOFactory.getDAOFactory(persistance).getRevueDAO().getAll());
	}
	
	@FXML
	public void validateRevue() {
		List<String> errors = new ArrayList<String>();
		
		// ID
		int id = 0;
		try {
			id = Integer.parseInt(el_inputIdRevue.getText());
		} catch(NumberFormatException error) {
			errors.add("L'id n'est pas au bon format");
		}
		
		// TITRE
		String titre = el_inputTitreRevue.getText().trim();
		
		if(titre.length() > 40) {
			errors.add("Titre trop long.");
		} else if(titre.length() == 0) {
			errors.add("Titre vide !");
		}		
		
		// DESCRIPTION
		String description = el_inputDescriptionRevue.getText();
		
		if(description.length() > 400) {
			errors.add("Description trop long");
		} else if(description.length() == 0) {
			errors.add("Description vide !");
		}
		
		// TARIF
		float tarif_float = 0;
		try {
			tarif_float = Float.parseFloat(el_inputTarifRevue.getText());
		} catch(NumberFormatException error) {
			errors.add("Le tarif n'est pas au bon format");
		}
		
		// PERIODICITE
		String periodicite_label = el_inputPeriodiciteRevue.getSelectionModel().getSelectedItem().toString();
		int periodicite_id = 0;
		try {
			List<Periodicite> periodicite = DAOFactory.getDAOFactory(persistance).getPeriodiciteDAO().getAll();
			for(Periodicite p : periodicite) {
				if(p.getLibelle().equals(periodicite_label)) {
					periodicite_id = p.getId_periodicite();
				}
			}
		} catch(Exception error) {
			errors.add("Aucune périodicté choisie.");
		}
		
		if(errors.isEmpty()) {
			Revue revue = new Revue(id, titre, description, tarif_float, revueVisuel, periodicite_id);
			if(DAOFactory.getDAOFactory(persistance).getRevueDAO().getById(id) == null) {
				DAOFactory.getDAOFactory(persistance).getRevueDAO().create(revue);
			} else {
				DAOFactory.getDAOFactory(persistance).getRevueDAO().update(revue);
			}
			
			updateRevueTable();
			loadPerioRevueBox();
			
			el_inputIdRevue.clear();
			el_inputTitreRevue.clear();
			el_inputDescriptionRevue.clear();
			el_inputTarifRevue.clear();
			el_imgRevue.setImage(null);
			revueVisuel = "";
			
		} else {
			String error = "";
			for(String s : errors) {
				error += s + "\n";
			}
			displayMessageBox(Alert.AlertType.ERROR, "Erreur lors de la saisie", "Un ou plusieurs champs sont mal remplis", error);
		}
	}

	@FXML
	public void editRevue() {
		Revue revue = el_tabRevue.getSelectionModel().getSelectedItem();
		if(revue == null) {
			displayMessageBox(Alert.AlertType.ERROR, "Erreur", "Vous devez sélectionner une revue !", "Aucune revue sélectionnée...");
		} else {
			el_inputIdRevue.setText(Integer.toString(revue.getId_revue()));
			el_inputTitreRevue.setText(revue.getTitre());
			el_inputDescriptionRevue.setText(revue.getDescription());
			el_inputTarifRevue.setText(Float.toString(revue.getTarif_numero()));
			revueVisuel = revue.getVisuel();
			el_imgRevue.setImage((revueVisuel.length() > 0) ? new Image(revueVisuel) : null);
		}
	}
	
	@FXML
	public void deleteRevue() {
		Revue revue = el_tabRevue.getSelectionModel().getSelectedItem();
		if(revue == null) {
			displayMessageBox(Alert.AlertType.ERROR, "Erreur", "Vous devez sélectionner une revue !", "Aucune revue sélectionnée...");
		} else {
			DAOFactory.getDAOFactory(persistance).getRevueDAO().delete(revue);
			updateRevueTable();
			loadPerioRevueBox();
		}
	}
	
	@FXML
	public void selectRevueVisuel() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choisir un visuel...");
		fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("All Images", "*.*"),
            new FileChooser.ExtensionFilter("JPG", "*.jpg"),
            new FileChooser.ExtensionFilter("PNG", "*.png")
        );
		File file = fileChooser.showOpenDialog(null);
		if(file != null) {
			try {
				revueVisuel = file.toURI().toURL().toString();
			} catch (MalformedURLException e) { }
			Image image = new Image(revueVisuel);			
			el_imgRevue.setImage(image);
		}
	}
	
	// REVUE END
	// -------------------------
	// CLIENT BEG
	
	@SuppressWarnings("unchecked")
	private void initClientTable() {
		TableColumn<Client, Integer> colID = new TableColumn<Client, Integer>("ID");
		colID.setCellValueFactory(new PropertyValueFactory<Client, Integer>("id_client"));
		
		TableColumn<Client, String> colNom = new TableColumn<Client, String>("Nom");
		colNom.setCellValueFactory(new PropertyValueFactory<Client, String>("nom"));
		
		TableColumn<Client, String> colPrenom = new TableColumn<Client, String>("Prenom");
		colPrenom.setCellValueFactory(new PropertyValueFactory<Client, String>("prenom"));
		
		TableColumn<Client, String> colVoie = new TableColumn<Client, String>("Voie");
		colVoie.setCellValueFactory(new PropertyValueFactory<Client, String>("voie"));
		
		el_tabClient.getColumns().setAll(colID, colNom, colPrenom, colVoie);
		
		List<Client> clients = DAOFactory.getDAOFactory(persistance).getClientDAO().getAll();
		if(clients != null) {
			el_tabClient.getItems().addAll(clients);
		}
	}
	
	private void updateClientTable() {
		el_tabClient.getItems().clear();
		el_tabClient.getItems().addAll(DAOFactory.getDAOFactory(persistance).getClientDAO().getAll());
	}
	
	@FXML
	public void validateClient() {
		List<String> errors = new ArrayList<String>();
		
		int id = 0;
		try {
			id = Integer.parseInt(el_inputIdClient.getText());
		} catch(NumberFormatException error) {
			errors.add("L'id n'est pas au bon format");
		}
		
		String nom = el_inputNomClient.getText().trim();
		
		if(nom.length() > 30) {
			errors.add("Nom trop long.");
		} else if(nom.length() == 0) {
			errors.add("Nom vide !");
		}
		
		String prenom = el_inputPrenomClient.getText().trim();
		
		if(prenom.length() > 30) {
			errors.add("Prenom trop long.");
		} else if(prenom.length() == 0) {
			errors.add("Prenom vide !");
		}
		
		String no_rue = Normalisation.no_voie(el_inputNorueClient.getText().trim());
		
		if(no_rue.length() > 8) {
			errors.add("No rue trop longue.");
		} else if(no_rue.length() == 0) {
			errors.add("No rue vide !");
		}
		
		String voie = Normalisation.Voie(el_inputVoieClient.getText().trim());
		
		if(voie.length() > 80) {
			errors.add("Voie trop longue.");
		} else if(voie.length() == 0) {
			errors.add("Voie vide !");
		}
		
		String code_postal = Normalisation.CodePostal(el_inputCodepostalClient.getText().trim());
		
		if(code_postal.length() > 10) {
			errors.add("Code postal trop long.");
		} else if(code_postal.length() == 0) {
			errors.add("Code postal vide !");
		}
		
		String ville = Normalisation.Ville(el_inputVilleClient.getText().trim());
		
		if(ville.length() > 30) {
			errors.add("Ville trop long.");
		} else if(ville.length() == 0) {
			errors.add("Ville vide !");
		}
		
		String pays = Normalisation.Pays(el_inputPaysClient.getText().trim());
		
		if(pays.length() > 30) {
			errors.add("Pays trop long.");
		} else if(pays.length() == 0) {
			errors.add("Pays vide !");
		}
		
		if(errors.isEmpty()) {
			Client client = new Client(id, nom, prenom, no_rue, voie, code_postal, ville, pays);
			if(DAOFactory.getDAOFactory(persistance).getClientDAO().getById(id) == null) {
				DAOFactory.getDAOFactory(persistance).getClientDAO().create(client);
			} else {
				DAOFactory.getDAOFactory(persistance).getClientDAO().update(client);
			}
			
			updateClientTable();
			loadPerioClientBox();
			
			el_inputIdClient.clear();
			el_inputNomClient.clear();
			el_inputPrenomClient.clear();
			el_inputNorueClient.clear();
			el_inputVoieClient.clear();
			el_inputCodepostalClient.clear();
			el_inputVilleClient.clear();
			el_inputPaysClient.clear();			
		} else {
			String error = "";
			for(String s : errors) {
				error += s + "\n";
			}
			displayMessageBox(Alert.AlertType.ERROR, "Erreur lors de la saisie", "Un ou plusieurs champs sont mal remplis", error);
		}
	}
	
	@FXML
	public void editClient() {
		Client client = el_tabClient.getSelectionModel().getSelectedItem();
		if(client == null) {
			displayMessageBox(Alert.AlertType.ERROR, "Erreur", "Vous devez sélectionner un client !", "Aucun client sélectionné...");
		} else {
			el_inputIdClient.setText(Integer.toString(client.getId_client()));
			el_inputNomClient.setText(client.getNom());
			el_inputPrenomClient.setText(client.getPrenom());
			el_inputNorueClient.setText(client.getNo_rue());
			el_inputVoieClient.setText(client.getVoie());
			el_inputCodepostalClient.setText(client.getCode_postal());
			el_inputVilleClient.setText(client.getVille());
			el_inputPaysClient.setText(client.getPays());
		}
	}
	
	@FXML
	public void deleteClient() {
		Client client = el_tabClient.getSelectionModel().getSelectedItem();
		if(client == null) {
			displayMessageBox(Alert.AlertType.ERROR, "Erreur", "Vous devez sélectionner un client !", "Aucun client sélectionné...");
		} else {
			DAOFactory.getDAOFactory(persistance).getClientDAO().delete(client);
			updateClientTable();
			loadPerioClientBox();
		}
	}
	
	@FXML
	public void importCSV() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choisir un fichier...");
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
		File file = fileChooser.showOpenDialog(null);
		List<Client> clients = CSVReader.parseClients(file.getAbsolutePath());
		clients.forEach(client -> {
			DAO<Client> clientDAO = DAOFactory.getDAOFactory(persistance).getClientDAO();
			if(clientDAO.getById(client.getId_client()) == null) {
				clientDAO.create(client);
			} else {
				clientDAO.update(client);
			}
		});
		displayMessageBox(Alert.AlertType.CONFIRMATION, "OK !", "Clients ajoutés !", "");
		updateClientTable();
	}
	
	// CLIENT END
	// -------------------------
	// ABONNEMENT BEG
	
	@SuppressWarnings("unchecked")
	private void initAboTable() {
		TableColumn<Abonnement, Integer> colIDClient = new TableColumn<Abonnement, Integer>("ID Client");
		colIDClient.setCellValueFactory(new PropertyValueFactory<Abonnement, Integer>("id_client"));
		
		TableColumn<Abonnement, Integer> colIDRevue = new TableColumn<Abonnement, Integer>("ID Revue");
		colIDRevue.setCellValueFactory(new PropertyValueFactory<Abonnement, Integer>("id_revue"));
		
		TableColumn<Abonnement, String> colDateDeb = new TableColumn<Abonnement, String>("Date debut");
		colDateDeb.setCellValueFactory(new PropertyValueFactory<Abonnement, String>("date_debut"));
		
		TableColumn<Abonnement, String> colDateFin = new TableColumn<Abonnement, String>("Date fin");
		colDateFin.setCellValueFactory(new PropertyValueFactory<Abonnement, String>("date_fin"));
		
		el_tabAbo.getColumns().setAll(colIDClient, colIDRevue, colDateDeb, colDateFin);
		
		List<Abonnement> abonnements = DAOFactory.getDAOFactory(persistance).getAbonnementDAO().getAll();
		if(abonnements != null) {
			el_tabAbo.getItems().addAll(abonnements);
		}
	}
	
	private void updateAboTable() {
		el_tabAbo.getItems().clear();
		el_tabAbo.getItems().addAll(DAOFactory.getDAOFactory(persistance).getAbonnementDAO().getAll());
	}
	
	@FXML
	public void validateAbo() {
		List<String> errors = new ArrayList<String>();
		
		int idClient = 0;
		try {
			String nomClient = el_inputClientAbo.getSelectionModel().getSelectedItem();
			String nom = nomClient.split(" ")[0],
					prenom = nomClient.split(" ")[1];
			List<Client> clients = DAOFactory.getDAOFactory(persistance).getClientDAO().getAll();
			for(Client client : clients) {
				if(client.getNom().equals(nom) && client.getPrenom().equals(prenom)) {
					idClient = client.getId_client();
				}
			}
		} catch(NumberFormatException error) {
			errors.add("Client invalide");
		}
		
		int idRevue = 0;
		try {
			String titre = el_inputRevueAbo.getSelectionModel().getSelectedItem();
			List<Revue> revues = DAOFactory.getDAOFactory(persistance).getRevueDAO().getAll();
			for(Revue revue : revues) {
				if(revue.getTitre().equals(titre)) {
					idRevue = revue.getId_revue();
				}
			}
		} catch(NumberFormatException error) {
			errors.add("Revue invalide");
		}
		
		String datedeb = el_inputDatedebAbo.getValue().toString();
		String datefin = el_inputDatefinAbo.getValue().toString();
		
		if(errors.isEmpty()) {
			Abonnement abo = new Abonnement(idClient, idRevue, datedeb, datefin);
			
			if(DAOFactory.getDAOFactory(persistance).getAbonnementDAO().getById(idClient, idRevue) == null) {
				DAOFactory.getDAOFactory(persistance).getAbonnementDAO().create(abo);
			} else {
				DAOFactory.getDAOFactory(persistance).getAbonnementDAO().update(abo);
			}
			
			updateAboTable();
			
			el_inputDatedebAbo.setValue(null);
			el_inputDatefinAbo.setValue(null);
		} else {
			String error = "";
			for(String s : errors) {
				error += s + "\n";
			}
			displayMessageBox(Alert.AlertType.ERROR, "Erreur lors de la saisie", "Un ou plusieurs champs sont mal remplis", error);
		}
	}
	
	@FXML
	public void editAbo() {
		Abonnement abo = el_tabAbo.getSelectionModel().getSelectedItem();
		if(abo == null) {
			displayMessageBox(Alert.AlertType.ERROR, "Erreur", "Vous devez sélectionner un abonnement !", "Aucun abonnement sélectionné...");
		} else {
			String client = DAOFactory.getDAOFactory(persistance).getClientDAO().getById(abo.getId_client()).getNom()
					+ " " + 
					DAOFactory.getDAOFactory(persistance).getClientDAO().getById(abo.getId_client()).getPrenom();
			
			SingleSelectionModel<String> selectionModel2 = el_inputClientAbo.getSelectionModel();
			selectionModel2.select(client);
			
			String revue = DAOFactory.getDAOFactory(persistance).getRevueDAO().getById(abo.getId_revue()).getTitre();
			
			SingleSelectionModel<String> selectionModel3 = el_inputRevueAbo.getSelectionModel();
			selectionModel3.select(revue);
			
			el_inputDatedebAbo.setValue(LocalDate.parse(abo.getDate_debut()));
			el_inputDatefinAbo.setValue(LocalDate.parse(abo.getDate_fin()));
		}
	}
	
	@FXML
	public void deleteAbo() {
		Abonnement abo = el_tabAbo.getSelectionModel().getSelectedItem();
		if(abo == null) {
			displayMessageBox(Alert.AlertType.ERROR, "Erreur", "Vous devez sélectionner un abonnement !", "Aucun abonnement sélectionné...");
		} else {
			DAOFactory.getDAOFactory(persistance).getAbonnementDAO().delete(abo);
			updateAboTable();
		}
	}
	
	// ABONNEMENT END
	// ----------------------------
	// PERIODICITE BEG
	
	private void loadPerioClientBox() {
		List<Client> clients = DAOFactory.getDAOFactory(persistance).getClientDAO().getAll();
		List<String> clientsNoms = new ArrayList<String>();
		
		clients.forEach(client -> {
			clientsNoms.add(client.getNom() + " " + client.getPrenom());
		});
		
		el_inputClientAbo.setItems(FXCollections.observableList(clientsNoms));
		SingleSelectionModel<String> selectionModel2 = el_inputClientAbo.getSelectionModel();
		selectionModel2.select(0);
	}
	
	private void loadPerioRevueBox() {
		List<Revue> revues = DAOFactory.getDAOFactory(persistance).getRevueDAO().getAll();
		List<String> revuesStr = new ArrayList<String>();
		revues.forEach(revue -> {
			revuesStr.add(revue.getTitre());
		});
		
		el_inputRevueAbo.setItems(FXCollections.observableList(revuesStr));
		SingleSelectionModel<String> selectionModel3 = el_inputRevueAbo.getSelectionModel();
		selectionModel3.select(0);
	}
	
	@SuppressWarnings("unchecked")
	private void initPerioTable() {
		TableColumn<Periodicite, Integer> colIDPerio = new TableColumn<Periodicite, Integer>("ID Periodicite");
		colIDPerio.setCellValueFactory(new PropertyValueFactory<Periodicite, Integer>("id_periodicite"));
		
		TableColumn<Periodicite, String> colLibelle = new TableColumn<Periodicite, String>("Libellé");
		colLibelle.setCellValueFactory(new PropertyValueFactory<Periodicite, String>("libelle"));
		
		el_tabPerio.getColumns().setAll(colIDPerio, colLibelle);
		
		List<Periodicite> periodicite = DAOFactory.getDAOFactory(persistance).getPeriodiciteDAO().getAll();
		if(periodicite != null) {
			el_tabPerio.getItems().addAll(periodicite);
		}
	}
	
	private void updatePerioTable() {
		el_tabPerio.getItems().clear();
		el_tabPerio.getItems().addAll(DAOFactory.getDAOFactory(persistance).getPeriodiciteDAO().getAll());
	}
	
	@FXML
	public void validatePerio() {
		List<String> errors = new ArrayList<String>();
		int id = 0;
		try {
			id = Integer.parseInt(el_inputIdPerio.getText());
		} catch(NumberFormatException error) {
			errors.add("L'id n'est pas au bon format");
		}
		
		String libelle = el_inputLabelPerio.getText();
		
		if(errors.isEmpty()) {
			Periodicite periodicite = new Periodicite(id, libelle);
			
			if(DAOFactory.getDAOFactory(persistance).getPeriodiciteDAO().getById(id) == null) {
				DAOFactory.getDAOFactory(persistance).getPeriodiciteDAO().create(periodicite);
			} else {
				DAOFactory.getDAOFactory(persistance).getPeriodiciteDAO().update(periodicite);
			}
			
			updatePerioTable();
			loadRevuePerioBox();
			
			el_inputIdPerio.clear();
			el_inputLabelPerio.clear();
		} else {
			String error = "";
			for(String s : errors) {
				error += s + "\n";
			}
			displayMessageBox(Alert.AlertType.ERROR, "Erreur lors de la saisie", "Un ou plusieurs champs sont mal remplis", error);
		}
	}
	
	@FXML
	public void editPerio() {
		Periodicite periodicite = el_tabPerio.getSelectionModel().getSelectedItem();
		if(periodicite == null) {
			displayMessageBox(Alert.AlertType.ERROR, "Erreur", "Vous devez sélectionner une périodicité !", "Aucune périodicité sélectionné...");
		} else {
			el_inputIdPerio.setText(Integer.toString(periodicite.getId_periodicite()));
			el_inputLabelPerio.setText(periodicite.getLibelle());
		}
	}
	
	@FXML
	public void deletePerio() {
		Periodicite periodicite = el_tabPerio.getSelectionModel().getSelectedItem();
		if(periodicite == null) {
			displayMessageBox(Alert.AlertType.ERROR, "Erreur", "Vous devez sélectionner une périodicité !", "Aucune périodicité sélectionné...");
		} else {
			DAOFactory.getDAOFactory(persistance).getPeriodiciteDAO().delete(periodicite);
			updatePerioTable();
			loadRevuePerioBox();
		}
	}
	// PERIODICITE END
}