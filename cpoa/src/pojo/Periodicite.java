package pojo;

public class Periodicite {
	private int id_periodicite;
	private String libelle;
	
	public Periodicite() {
		this.id_periodicite = 0;
		this.libelle = "";
	}
	
	public Periodicite(int id_periodicite) {
		this.id_periodicite = id_periodicite;
	}

	public Periodicite(String libelle2) {
		this.libelle = libelle2;
	}
	
	public Periodicite(int id_periodicite, String libelle) {
		this.id_periodicite = id_periodicite;
		this.libelle = libelle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id_periodicite;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Periodicite other = (Periodicite) obj;
		if (id_periodicite != other.id_periodicite)
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}

	public int getId_periodicite() {
		return id_periodicite;
	}

	public void setId_periodicite(int id_periodicite) {
		this.id_periodicite = id_periodicite;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
}
