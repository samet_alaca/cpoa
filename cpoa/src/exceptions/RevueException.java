package exceptions;

public class RevueException extends Exception {

	private String cause = "Unknown error";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1351878242467562722L;
	
	public RevueException(String cause) {
		this.cause = cause;
	}
	
	@Override
	public String getMessage() {
		return this.cause;
	}
}
