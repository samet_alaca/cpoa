-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 07 nov. 2017 à 21:01
-- Version du serveur :  5.7.19
-- Version de PHP :  7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cpoa`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

DROP TABLE IF EXISTS `abonnement`;
CREATE TABLE IF NOT EXISTS `abonnement` (
  `id_client` int(11) NOT NULL,
  `id_revue` int(4) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  PRIMARY KEY (`id_client`,`id_revue`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `abonnement`
--

INSERT INTO `abonnement` (`id_client`, `id_revue`, `date_debut`, `date_fin`) VALUES
(1, 1, '2017-11-07', '2017-11-14'),
(1, 2, '2017-11-07', '2017-10-31');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `no_rue` varchar(8) DEFAULT NULL,
  `voie` varchar(80) DEFAULT NULL,
  `code_postal` varchar(10) DEFAULT NULL,
  `ville` varchar(30) DEFAULT NULL,
  `pays` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id_client`, `nom`, `prenom`, `no_rue`, `voie`, `code_postal`, `ville`, `pays`) VALUES
(1, 'Alaca', 'Samet', '23,', 'Rue Kennedy', '57280', 'Mazières-lès-Metz', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `periodicite`
--

DROP TABLE IF EXISTS `periodicite`;
CREATE TABLE IF NOT EXISTS `periodicite` (
  `id_periodicite` int(2) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(20) NOT NULL,
  PRIMARY KEY (`id_periodicite`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `periodicite`
--

INSERT INTO `periodicite` (`id_periodicite`, `libelle`) VALUES
(1, 'Quotidien'),
(2, 'Hebdomadaire'),
(3, 'Mensuel'),
(4, 'Trimestriel'),
(5, 'Semestriel'),
(6, 'Annuel');

-- --------------------------------------------------------

--
-- Structure de la table `revue`
--

DROP TABLE IF EXISTS `revue`;
CREATE TABLE IF NOT EXISTS `revue` (
  `id_revue` int(4) NOT NULL AUTO_INCREMENT,
  `titre` varchar(40) NOT NULL,
  `description` varchar(400) NOT NULL,
  `tarif_numero` float DEFAULT NULL,
  `visuel` varchar(200) DEFAULT NULL,
  `id_periodicite` int(2) NOT NULL,
  PRIMARY KEY (`id_revue`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `revue`
--

INSERT INTO `revue` (`id_revue`, `titre`, `description`, `tarif_numero`, `visuel`, `id_periodicite`) VALUES
(1, 'Titre Revue 1', 'Description Revue 1', 1, '', 1),
(2, 'Titre Revue 2', 'Description Revue 2', 2, '', 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
